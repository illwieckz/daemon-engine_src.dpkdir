Basic data package for the Dæmon Engine
=======================================

This is a package shipping basic assets required by the Dæmon engine

You need this package until those issues are fixed:

- https://github.com/DaemonEngine/Daemon/pull/15
- https://github.com/DaemonEngine/Daemon/issues/71

This package is a reduced version of:

- https://github.com/UnvanquishedAssets/unvanquished_src.dpkdir/

Related copyright and license apply.
